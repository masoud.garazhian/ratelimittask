﻿using Newtonsoft.Json;
using RateLimiterTask.Models;
using StackExchange.Redis.Extensions.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateLimiterTask.Repository
{
    public class RedisRepository : IRedisRepository
    {
        private readonly IRedisCacheClient redisCacheClient;

        public RedisRepository(IRedisCacheClient redisCacheClient)
        {
            this.redisCacheClient = redisCacheClient;
        }

        public async Task<bool> Insert(string key, object value, DateTimeOffset dateTimeOffset)
        {
            return await redisCacheClient.GetDbFromConfiguration().AddAsync<string>(key, JsonConvert.SerializeObject(value), dateTimeOffset);
        }

        public async Task<string> Get(string key)
        {
            return await redisCacheClient.GetDbFromConfiguration().GetAsync<string>(key);
        }

        public async Task<bool> Delete(string key)
        {
            return await redisCacheClient.GetDbFromConfiguration().RemoveAsync(key);
        }
 

    }
}
