﻿using System;
using System.Threading.Tasks;

namespace RateLimiterTask.Repository
{
    public interface IRedisRepository
    {
        Task<bool> Delete(string key);
        Task<string> Get(string key);
        Task<bool> Insert(string key, object value, DateTimeOffset dateTimeOffset); 
    }
}