﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RateLimiterTask.Models;
using RateLimiterTask.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RateLimiterTask.Services
{
    public class RateLimiterService : IRateLimiterService
    {
        private readonly IRedisRepository redisRepo;

        public RateLimiterService(IConfiguration configuration,
                                    IRedisRepository redisRepo)
        {
            this.redisRepo = redisRepo;
        } 

        public async Task<bool> CheckIp(string ip, string url)
        {
            var key = ip + AppConstants.SiteDomain + url;

            var limit = FindMatchRegex(url);

            if (limit == null) return true;

            var content = await redisRepo.Get(key);

            if (content == null)
            {
                await redisRepo.Insert(key, " :D Called ", CalculateDateTimeOffset(limit));
                return true;
            }
            return false;
        }

        private DateTimeOffset CalculateDateTimeOffset(Limit limit)
        {
            var splitedStr = limit.TimeFrame.Split('/');
            switch (splitedStr[1].ToString())
            {
                case "sec": return DateTimeOffset.Now.AddSeconds(int.Parse(splitedStr[0].ToString()));
                case "min": return DateTimeOffset.Now.AddMinutes(int.Parse(splitedStr[0].ToString()));
                case "hr": return DateTimeOffset.Now.AddHours(int.Parse(splitedStr[0].ToString()));
                default:
                    throw new NotImplementedException("Time frame doesn't exist");
            }
        }

        private Limit FindMatchRegex(string url)
        {
            url = AppConstants.SiteDomain + url;
            foreach (var item in AppConstants.RateLimiterConfigs.Limits)
            {
                Regex reg = new Regex(item.Pattern);
                if (reg.IsMatch(url))
                {
                    return item;
                }
            }
            return null;
        }
    }
}
