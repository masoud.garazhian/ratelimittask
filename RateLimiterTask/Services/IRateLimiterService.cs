﻿using System.Threading.Tasks;

namespace RateLimiterTask.Services
{
    public interface IRateLimiterService
    {
        Task<bool> CheckIp(string ip, string url);
    }
}