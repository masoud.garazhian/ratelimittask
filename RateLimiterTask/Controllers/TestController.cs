﻿using Microsoft.AspNetCore.Mvc;
using RateLimiterTask.Models;
using StackExchange.Redis.Extensions.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace RateLimiterTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly IRedisCacheClient redisCacheClient;

        public TestController(IRedisCacheClient redisCacheClient)
        {
            this.redisCacheClient = redisCacheClient;
        }

        // GET: api/<TestController>
        [HttpGet]
        public IEnumerable<string> Get()
        { 
            return new string[] { "value1", "value2" };
        }

        // GET api/<TestController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<TestController>
        [HttpPost]
        public async Task<IActionResult> Post(string value) 
        {
            Guid guid = Guid.NewGuid();
            await redisCacheClient.GetDbFromConfiguration().AddAsync<string>(guid.ToString(), value + " Value", DateTimeOffset.Now.AddMinutes(AppConstants.ExpireAt));
            return Ok(await redisCacheClient.GetDbFromConfiguration().GetAsync<string>(guid.ToString()));
        }

        // PUT api/<TestController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<TestController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
