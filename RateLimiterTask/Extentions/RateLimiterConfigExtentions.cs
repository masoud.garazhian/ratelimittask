﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RateLimiterTask.Models;

namespace RateLimiterTask.Extentions
{
    public static class RateLimiterConfigExtentions
    {
        public static void RateLimiterConfig(this IServiceCollection services, IConfiguration Configuration)
        {
            AppConstants.RateLimiterConfigs = Configuration.GetSection("RateLimiterConfig").Get<RateLimiterConfig>();
        }
    }

}
