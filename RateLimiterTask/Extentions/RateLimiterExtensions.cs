﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using RateLimiterTask.Middleware;

namespace RateLimiterTask.Extentions
{
    public static class RateLimiterExtensions
    {
        public static void UseRateLimiter(this IApplicationBuilder builder)
        {
            builder.UseMiddleware<RateLimiterMiddleware>(); 
        }
    }
}
