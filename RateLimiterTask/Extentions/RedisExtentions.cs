﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RateLimiterTask.Models;
using StackExchange.Redis.Extensions.Core.Configuration;
using StackExchange.Redis.Extensions.Newtonsoft;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateLimiterTask.Extentions
{
    public static class RedisExtentions
    {
        public static void AddRedisExtensions(this IServiceCollection services, IConfiguration Configuration)
        {
            services.AddStackExchangeRedisExtensions<NewtonsoftSerializer>((options) =>
            {
                return Configuration.GetSection("Redis").Get<RedisConfiguration>();
            });

            AppConstants.ExpireAt = Configuration.GetSection("Redis").Get<MyRedisConfiguration>().ExpireAt;
        }
    }
}
