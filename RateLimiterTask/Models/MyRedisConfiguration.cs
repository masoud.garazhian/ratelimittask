﻿using StackExchange.Redis.Extensions.Core.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateLimiterTask.Models
{
    public class MyRedisConfiguration : RedisConfiguration
    {
        public int ExpireAt { get; set; }
    }
}
