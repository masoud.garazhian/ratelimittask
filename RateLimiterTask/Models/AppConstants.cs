﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateLimiterTask.Models
{
    public static class AppConstants
    {
        public static int ExpireAt { get; set; } = 86400;
        public static string SiteDomain { get; set; } = "www.zoomit.com";
        public static RateLimiterConfig RateLimiterConfigs { get; set; }
    }
}
