﻿using System.Collections.Generic;

namespace RateLimiterTask.Models
{
    public class RateLimiterConfig
    {
        public List<Limit> Limits { get; set; }

    }

    public class Limit
    {
        public string Pattern { get; set; } 
        public string TimeFrame { get; set; }
    }
}
