﻿using Microsoft.AspNetCore.Http;
using RateLimiterTask.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateLimiterTask.Middleware
{
    public class RateLimiterMiddleware
    {
        private readonly RequestDelegate next;
        private readonly IRateLimiterService rateLimiterService;

        public RateLimiterMiddleware(RequestDelegate next,
            IRateLimiterService rateLimiterService)
        {
            this.next = next;
            this.rateLimiterService = rateLimiterService;
        }

        public async Task Invoke(HttpContext context)
        {
            var result = await rateLimiterService.CheckIp(context.Connection.RemoteIpAddress.ToString(), context.Request.Path.Value);//context.Request.Request.Url.AbsoluteUri
            if (!result)
            {
                context.Response.StatusCode = 429;
            }
            else
                await next(context);

        }

    }
}
