﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RateLimiterTask.Test
{
    [Microsoft.VisualStudio.TestTools.UnitTesting.TestClass]
    public class UnitTestService : BaseTest
    {
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethod]
        public void MyTestMethod()
        {
            rateLimiterService.CheckIp("127.0.0.1", "/api/Test");
            var res2 = rateLimiterService.CheckIp("127.0.0.1", "/api/Test").Result;
            Assert.AreEqual(res2, false);
        }
    }
}
