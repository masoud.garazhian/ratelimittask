
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using NUnit.Framework;
using Xunit;

namespace RateLimiterTask.Test
{
    public class TestControllerTest : IClassFixture<CustomWebApplicationFactory<Startup>>
    {

        private readonly HttpClient _client;
        private readonly CustomWebApplicationFactory<Startup> _factory;

        public TestControllerTest(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
            _client = _factory.CreateClient(new WebApplicationFactoryClientOptions
            {
                AllowAutoRedirect = false
            });
        }

        [Fact]
        public async Task GET_retrieves_weather_forecast()
        {
            var response = await _client.GetAsync("/api/Test");

            Assert.AreEqual(response.StatusCode, 429);
        }

        [Xunit.Theory]
        [InlineData("/api/Test", "text/plain; charset=utf-8")]
        public async Task Get_EndpointsReturnSuccessAndCorrectContentType(string url, string expected)
        {

            var response = await _client.GetAsync(url);

            var res = response.EnsureSuccessStatusCode();
             
            Assert.AreEqual(res.StatusCode, 429);

        }

    }
}