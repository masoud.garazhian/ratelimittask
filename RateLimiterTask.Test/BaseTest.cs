﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System.Threading;
using System.Globalization;
using RateLimiterTask.Services;

namespace RateLimiterTask.Test
{

    public class BaseTest
    {
        protected IWebHost webHost;
        protected IRateLimiterService rateLimiterService;



        public BaseTest()
        {
            webHost = WebHost
            .CreateDefaultBuilder()
            .UseUrls("http://localhost:52202")
            .UseStartup<Startup>()
            .Build();
            webHost.Start();

            rateLimiterService = webHost.Services.GetService<IRateLimiterService>();
        }


    }
}
