## Introduction

This is a simple show for Rate Limiting and Throttle of the api requests, a middleware, this test made for representing code and show config cases in appsettings.json\

this project and library meant to use in a gateway api project to control the limits of apis in microservice, its a middleware that checks every request in pipelne to secure the apis in the controller.
 
 

## Deploy

 Same as other web apis its easy to deploy in IIS, NginX and etc... 
 you have to publish and copy the files to the directory provided before

 for middle ware first thing is to you have to add middleware codes to ur project 
 then the startup setting must be add and pipeline middleware must be set as it shown
 after that you have to configure appsettings.json file to your needs
 and set it to every controller routs you exposed or set the general settings as it shown 
 
## Configure appsettings.json

you can use Regex and static Routes 
RateLimiterConfig section 
static endpoints and regex endpoints
```
 "RateLimiterConfig": {
    "Limits": [
      {
        "Pattern": "www.zoomit.com/car/*", 
        "TimeFrame": "1/sec"
      },
      {
        "Pattern": "www.zoomit.com/iphone/*", 
        "TimeFrame": "2/min"
      },
      {
        "Pattern": "www.zoomit.com/api/*",
        "TimeFrame": "24/hr"
      }
    ] 
  }
```
  
 
it means user can request N request in M second time period

the time periods can be:
``` 
        "Period": "1/sec", // as seconds
        "Period": "2/min",// as minates
        "Period": "24/hr", // as hours 
```

 
 By Masoud.garazhian@gmail.com